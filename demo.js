
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}




$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#Spol").val(podatki[2]);
    $("#Starost").val(podatki[3]);
    $("#dodajVitalnoTelesnaVisina").val(podatki[4]);
    $("#dodajVitalnoTelesnaTeza").val(podatki[5]);
    $("#aktivnost").val(podatki[6]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});

	google.charts.load('current', {packages: ['corechart', 'bar']});
	google.charts.setOnLoadCallback(Izracunaj());

function Izracunaj(){
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var starost = $("#Starost").val();
	var spol = $("#Spol").val();
	var visina= $("#dodajVitalnoTelesnaVisina").val();
	var teza = $("#dodajVitalnoTelesnaTeza").val();
	var aktivnost = $("#aktivnost").val();
	
/*	alert(ime +" "+priimek+" "+starost+" "+spol+" "+visina+" "+teza+" "+aktivnost);   */
	document.getElementById("vrniimepriimek").innerHTML = ime + " "+priimek;
	
	var BMI = teza / (visina*visina)*10000;
	document.getElementById("vrniBMI").innerHTML = parseFloat(BMI).toFixed(2);
	
	var vrednostBMI = teza / (visina*visina)*10000;
	var status = 0;
	if ( vrednostBMI < 18.50) {
    	status = "prelahki";
    	document.getElementById("statusBMI").innerHTML = status;
    	document.getElementById("statusBMI").style.color = "#FFA500";
	}
	else if (vrednostBMI < 25) {
    	status = "normalna teža";
    	document.getElementById("statusBMI").innerHTML = status;
    	document.getElementById("statusBMI").style.color = "#008000";
	}
	else if (vrednostBMI < 30){
    	status = "pretežki";
    	document.getElementById("statusBMI").innerHTML = status;
    	document.getElementById("statusBMI").style.color = "#FFA500";
	}
	else{
		status = "debeli";
		document.getElementById("statusBMI").innerHTML = status;
		document.getElementById("statusBMI").style.color = "#FF0000";
	}
	
	var BMR = 0;
	var ideal = 0;
	if(spol == "Moski"){
		BMR = 10 * teza + 6.25 * visina - 5 * starost + 5;
		document.getElementById("vrniBMR").innerHTML = BMR;
	}
	else{
		BMR = 10 * teza + 6.25 * visina - 5 * starost - 161;
		document.getElementById("vrniBMR").innerHTML = BMR;
	}
	
	
	var izgubatezepol = 0;
	var izgubatezeena = 0;
	var izgubatezenpol = 0;
	var izgubatezenena = 0;
	
	
	if(spol == "Moski"){
		if(aktivnost == "Športno nisem aktiven"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost + 5 - 100;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost + 5 - 500;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 900;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost + 5 + 1400;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "1-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 300;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost + 5 - 200;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 1300;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost + 5 + 1500;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "2-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 600;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost + 5 + 200;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 1800;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost + 5 + 2000;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "3-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 750;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost + 5 - 50;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 2000;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost + 5 + 2300;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "4-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 900;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost + 5 + 100;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 2100;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost + 5 + 2400;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "5-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 1000;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost + 5 + 250;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost + 5 + 2300;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost + 5 + 3000;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
	}
	else{
		if(aktivnost == "Športno nisem aktiven"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost - 161 - 100;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost - 161 - 400;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 1000;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost - 161 + 1500;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "1-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 100;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost - 161 - 250;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 350;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost - 161 + 500;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "2-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 200;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost - 161 - 100;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 500;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost - 161 + 750;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "3-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 600;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost - 161 - 75;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 1500;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost - 161 + 1700;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "4-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 800;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost - 161 + 75;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 1700;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost - 161 + 2000;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
		else if(aktivnost == "5-krat na teden"){
			izgubatezepol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 900;
			document.getElementById("pol").innerHTML = izgubatezepol;
			izgubatezeena = 10 * teza + 6.25 * visina - 5 * starost - 161 + 200;
			document.getElementById("ena").innerHTML = izgubatezeena;
			izgubatezenpol = 10 * teza + 6.25 * visina - 5 * starost - 161 + 2000;
			document.getElementById("npol").innerHTML = izgubatezenpol;
			izgubatezenena = 10 * teza + 6.25 * visina - 5 * starost - 161 + 2500;
			document.getElementById("nena").innerHTML = izgubatezenena;
		}
	}
	
	  var data = google.visualization.arrayToDataTable([
        ['Poraba', 'Poraba kcal',],
        ['BMR',BMR],
        ['Izguba 0,5kg na teden', izgubatezepol],
        ['Izguba 1kg na teden',izgubatezeena],
        ['Pridobitev 0,5kg na teden', izgubatezenpol],
        ['Pridobitev 1kg na teden', izgubatezenena]
      ]);

      var options = {
        title: 'Poraba kalorij ',
        chartArea: {width: '50%'},
        hAxis: {
          title: 'Maksimalni Kcal za 1 dan',
          minValue: 0
        },
        vAxis: {
          title: 'Poraba'
        }
      };

      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

      chart.draw(data, options);
}
